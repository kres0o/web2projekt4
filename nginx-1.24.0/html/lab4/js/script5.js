document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM is ready.');

    // Example: Add interactivity
    const welcomeMessage = document.createElement('p');
    welcomeMessage.textContent = 'Thank website!';
    document.body.appendChild(welcomeMessage);
});