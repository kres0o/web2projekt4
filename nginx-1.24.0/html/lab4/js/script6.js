document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM is ready.');

    // Example: Add interactivity
    const welcomeMessage = document.createElement('p');
    welcomeMessage.textContent = 'Thank you               for                 visiting the website!';
    document.body.appendChild(welcomeMessage);
});