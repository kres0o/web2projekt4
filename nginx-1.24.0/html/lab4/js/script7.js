document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM is ready.');

    // Example: Add a new paragraph dynamically
    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'was added dynamically using JavaScript!';
    document.body.appendChild(newParagraph);

    // Add your additional JavaScript logic here
});